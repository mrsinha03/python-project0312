from ast import operator


first_num = input("Enter First Number : ")
operators = input("Choose any of these operator for calculate (+, -, /, %) : ")
second_num = input ("Enter Second Number : ")

if operators == "+":
    print(int(first_num) + int(second_num))
elif operators == "-":
    print(int(first_num) - int(second_num))
elif operators == "/":
    print(int(first_num) / int(second_num))
elif operators == "%":
    print(int(first_num) % int(second_num))    

else: 
    print("Invalid Operation")


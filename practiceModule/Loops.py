# while LOOP

i = 6
while i >= 0:
    print(i * "*")
    i = i - 1

i = 0
while i <= 6:
    print(i * "*")
    i = i + 1